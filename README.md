## Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)
Fuentes de información:
- [Programación Declarativa. Orientaciones. (1998)]
- [Programación Declarativa. Orientaciones y pautas para el estudio. (1999)]
- [Programación Declarativa. Orientaciones. (2001)]

```plantuml
@startmindmap
*[#LightBlue] **Programación Declarativa**
 *_ ¿Qué es?
  * Un paradigma de programación que surge como reacción a unos problemas que \ntrae consigo la programación clásica
   *_ como
    * Cuellos de botella intelectuales
     *_ Obliga al programador a tener en cuenta más detalles de los necesarios para ejecutar un programa
    * Especifidad de datos o algoritmos complejos
     *_ Se basa en la asignación y la modificación en detalle paso a paso
    * Estricta parte burocrática
     *_ el programador está obligado a dar muchos detalles
  * Hace énfasis en los aspectos creativos de la programación 
 *_ ¿En qué consiste?
  * En abandonar las secuencias de ordenes que gestionan la memoria del ordenador 
  * En tomar una perspectiva más de darle explicaciones (al ordenador) de los algoritmos con los que se pretende resolver los problemas
 *[#LightBlue] El ordenador 
  * Se encarga de todos los aspectos de la gestión de memoria
  * Se sigue asumiendo que es una máquina "tonta"
 *_ Existen variantes como
  * Programación Lógica
   *_ características
    * El compilador es un motor de inferencia
    * Se pueden definir reglas
    * nos saltamos la relación entre argumentos y resultados
   *_ utiliza
    * logica de predicados
    * reglas
   *_ Ventajas
    * Permite procesos inversos entre argumentos y resultados
    * Permite ser más declarativos
   *_ lenguajes como
    * Prolog
  * Programación Funcional
   *_ características
    * existencia de funciones de orden superior
    * aplicar programas a datos y programas a programas
    * se pierde la distinción funcional entre datos y programas
   *_ utiliza reglas como
    * Reescritura
    * Simplificación de expresiones
   *_ con enfoque
    * Matemático
    * en funciones
   *_ Ventajas
    * Función sobre función 
     *_ Las funciones se pueden ejecutar sobre otras funciones programadas 
    * Evaluación perezosa
     *_ consiste en
      * Calcular una expresión hasta que su valor sea requerido
     *_ Ventajas
      * El incremento en el rendimiento al evitar cálculos innecesarios
      * La capacidad de construir estructuras de datos potencialmente infinitas
      * La capacidad de definir estructuras de control como abstracciones, en lugar de operaciones primitivas
   *_ lenguajes como
    * Lisp
 *[#LightBlue] Objetivos y Ventajas 
  * Liberarse de las asignaciones y del control de memoria del ordenador
  * Conseguir programas más cortos en menos tiempo
  * Programas más eficientes
  *_ Mayor facilidad para
   * Codificar
   * Depurar
 *[#LightBlue] Desventajas
  * un nivel de abstracción **demasiado** alto se ve reflejado en un rendimiento pobre
  * pasar de un paradigma a otro es trivial en cuanto a definir la forma de pensar
@endmindmap
```

## Lenguaje de Programación Funcional (2015) 
Fuentes de información:
[Lenguaje de Programación Funcional. (2015)]

```plantuml
@startmindmap
*[#LightBlue] **Programación funcional**
 *_ ¿Qué es?
  * Paradigma de programación
   *_ que es
    * El modelo de computación
    * La forma en que se abordan problemas
 *_ Basado en
  * **Memoización**
   *_ no confundir con
    * Memo**r**ización
     * Capacidad de recordar datos
   *_ es
    * Almacenar el valor de una expresión cuya evaluación ya ha sido realizada
   * Evita volver a realizar pasos ya evaluados
   *_ data de
    * 1968
     *_ propuesto por
      * Donald Michie  
  * Currificación
   *_ es
    * Técnica para transformar una función de múltiples argumentos en
     * Una secuencia de funciones de un solo argumento
   *_ se utiliza en la programación funcional
    * Para utilizar funciones como argumentos de otras funciones
  * Cálculo Lambda
   *_ es
    * Un sistema computacional muy potente
     *_ surge de
      * El intento de estudiar un sistema para la aplicación de funciones de recursividad
  *_ creado en
   * 1930
    *_ por
     * Stephen Kleene
 *_ Variantes
  * Programación Orientada a Objetos
   *_ compuesto de
    * Trozos de código
     *_ que
      * Componen instrucciones secuenciales
      * Son objetos que interactúan entre sí
  * Lógica simbólica
   *_ es
    * La base o raíz del paradigma lógico
     *_ que funciona con
      * Inferencia
 *_ es
  * Un tipo de programación declarativa
   *_ como
    * Programación lógica
     *_ características
      * El compilador es un motor de inferencia
      * Se pueden definir reglas
      * nos saltamos la relación entre argumentos y resultados
     *_ utiliza
      * logica de predicados
      * reglas
     *_ Ventajas
      * Permite procesos inversos entre argumentos y resultados
      * Permite ser más declarativos
 *_ difiere de
  * Programación imperativa
   *_ Se define por
    * Colecciones de datos
    * Colecciones de instrucciones
    * Instrucciones se ejecutan 
 *[#LightBlue] Características
  * Se basa completamente en funciones
  * Recibe datos o funciones como entrada
  * Tiene más de una salida
  * Devuelve datos o funciones
  * Tiene funciones de orden superior
  * Se pueden aplicar funciones a datos y funciones a funciones
  * Se pierde la distinción entre datos y funciones
 *[#LightBlue] Reglas
  * Reescritura
  * Simplificación de expresiones
 *[#LightBlue] Ventajas
  * Liberarse de las asignaciones y del control de memoria del ordenador
  * Conseguir programas más cortos en menos tiempo
  * Programas más eficientes
  *_ Mayor facilidad para
   * Codificar
   * Depurar
 *[#LightBlue] Desventajas
  * un nivel de abstracción **demasiado** alto se ve reflejado en un rendimiento pobre
  * pasar de un paradigma a otro es trivial en cuanto a definir la forma de pensar

@endmindmap
```
##
Herramientas utilizadas:
- [Markdown]
- [PlantUML]
- [Mindmap]


_Autor: [LopezCruzRaul @ GitLab](https://gitlab.com/LopezCruzRaul)_



[//]: # (Se enlistan las referencias a continuación)
[Programación Declarativa. Orientaciones. (1998)]: https://canal.uned.es/video/5a6f2c66b1111f54718b4911
[Programación Declarativa. Orientaciones y pautas para el estudio. (1999)]: https://canal.uned.es/video/5a6f2c5bb1111f54718b488b
[Programación Declarativa. Orientaciones. (2001)]: https://canal.uned.es/video/5a6f2c42b1111f54718b4757
[Lenguaje de Programación Funcional. (2015)]: https://canal.uned.es/video/5a6f4bf3b1111f082a8b4708
[Markdown]: https://docs.gitlab.com/ee/user/markdown.html#diagrams-and-flowcharts
[PlantUML]: https://docs.gitlab.com/ee/administration/integration/plantuml.html
[Mindmap]: https://plantuml.com/es/mindmap-diagram

